:set number
:set noswapfile
:syntax on
:set scrolloff=10
:set cursorline
:set encoding=UTF-8
:set complete+=kspell
:set completeopt=menuone,longest
:set mouse=

call plug#begin('~/.nvim/plugged')
"Plug 'vim-ruby/vim-ruby' " adding special ruby runtime files

"Plug 'vim-scripts/AutoComplPop'

" colorschemes
Plug 'flazz/vim-colorschemes'
Plug 'ghifarit53/tokyonight-vim'
Plug 'jayden-chan/base46.nvim'

Plug 'fatih/vim-go' " for the go lang
Plug 'scrooloose/syntastic' " syntax tricks
Plug 'vim-airline/vim-airline' " pretty airline
Plug 'scrooloose/nerdtree' " nerd tree
Plug 'raimondi/delimitmate' " autoquotes

call plug#end()


"let g:airline_left_sep = '▶'
"let g:airline_left_alt_sep = '»'
"let g:airline_right_sep = '◀'
"let g:airline_right_alt_sep = '«'

let mapleader = ","

set termguicolors

let g:tokyonight_style = 'night' " available: night, storm
let g:tokyonight_enable_italic = 1

let g:go_highlight_function_calls = 1
let g:go_highlight_types = 1
let g:go_highlight_functions = 1

" colorscheme gruvbox
colorscheme tokyonight

map to <esc>:tab new<enter>
map <C-t> <esc>:NERDTreeToggle<enter>
map tt <esc>:tabNext<enter>
map td <esc>:wq<enter>

source $VIMRUNTIME/mswin.vim

"source $VIMRUNTIME/syntax/cpp.vim
"source $VIMRUNTIME/indent/cpp.vim
