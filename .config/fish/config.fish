bash ~/.fishprofile
set fish_greeting
alias gc="git clone"

function fish_title
    echo "terminal";
end


function fish_prompt
    printf '%s[' (set_color $fish_color_cwd)
    printf '%s' (prompt_pwd)
    printf ']%s$ ' (set_color normal)
end
