# MYDWM

<h2>Screenshots</h2>

<p align="center">
  <img src="screenshot1.png" width="500" title="hover text">
  <img src="screenshot2.png" width="500" title="hover text">
  <img src="screenshot3.png" width="500" title="hover text">
</p>

<h2>Dependencies</h2>

```
libXft libXft-devel libxcb libxcb-devel make gcc git (To clone repo and compile dwm)
feh (For wallpaper) xorg (To start dwm from tty) picom (For transparency)
dmenu (Dwm's standart application manger) xsetroot
```
<h2>Installation</h2>

Installation by install.sh script

```
git clone https://codeberg.org/Monderlaan/mydwm
cd mydwm
chmod +x install.sh
./install.sh
```

or manually:

```
# Clonning the repository and compiling dwm
git clone https://codeberg.org/Monderlaan/mydwm
cd mydwm
sudo make clean install
```
Start dwm
If you want to start dwm from tty you will need xorg

Now you need to replace wallpaper.jpg with
your wallpaper (File must start with "wallpaper")

Also ~/.fehbg must contain full path to wallpaper (/home/(user)/wallpaper.(extension))

```
# Setting up dwm, picom and feh
mv wallpaper* ~
mv .fehbg ~
mv .dwmstartup ~
chmod +x ~/.fehbg
chmod +x ~/.dwmstartup
```

If you want to start dwm from tty
will need execute this command:

```
echo "~/.fehbg; ~/.dwmstartup; exec dwm" > ~/.xinitrc
```

Starting from greeter (you need to do the previous steps)

```
sudo mv dwm.desktop /usr/share/xsessions/
```

Then you can choose dwm in your greeter

<h2>Keybindings</h2>

```
Win+D dmenu
Win+Shift+S exit
Win+Shift+A kill
Win+(q, w, e, r, 1, 2, 3, 4, 5) move between workspaces
Win+Shift+(q, w, e, r, 1, 2, 3, 4, 5) move windows between workspaces
Win+Shift+Enter launch st terminal
Win+B show/hide bar
Win+(Left/Right) switch to Next/Previous window
Win+(H/L) increase/decrease master size value
Win+(I/O) move/remove window on master
Win+0 view all windows on screen
Win+Shift+0 make focused window appear on all
Win+T tiled mode
Win+F floating mode
Win+M monocle mode
```
