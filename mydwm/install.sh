#!/usr/bin/bash

sudo echo ""

echo "Do you want to compile dwm? Y/N"
read compile

if [ $compile = "y" ]
then
echo "Compiling dwm...."
sudo make clean install
else
chmod +x dwm
sudo mv dwm /usr/local/bin
fi

mv wallpaper* ~
mv .fehbg ~
mv .dwmstartup ~
chmod +x ~/.fehbg
chmod +x ~/.dwmstartup

echo "Do you use greeter? Y/N"
read greeter
if [ $greeter = "y" ]
then
sudo mv dwm.desktop /usr/share/xsessions/
else
echo "~/.fehbg; ~/.dwmstartup; exec dwm" > ~/.xinitrc
echo "Use startx command to start dwm"
fi

echo "done!"
