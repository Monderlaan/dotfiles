<h1>MYST</h1>

<p align="center">
  <img src="screenshot.png" width="500" title="hover text">
</p>

<h2>Dependencies</h2>

```
libXft libXft-devel pkg-config patch make
```

<h2>Installation</h2>

by install.sh script:

```
git clone https://codeberg.org/Monderlaan/myst
cd myst
chmod +x install.sh
./install.sh
```

or manually:

```
#Clonning the repository
git clone https://codeberg.org/Monderlaan/myst
cd myst
#Applying the patches
patch < tokyonight-patch.diff
patch -p1 < anysize-patch.diff
#Compiling st
sudo make clean install
```
